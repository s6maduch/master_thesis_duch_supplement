#!/usr/bin/env python3

#  Developed by Matthias Duch.

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

from tkinter import *
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler

import os
import sys
import time
import math

import tkinter as tk
import tkinter.ttk as ttk



a_colors = ['blue', 'green', 'orange','orange'] # Updated version without red. As we now your see connected and disconnected devices
states = ['Never connected','Active','Regularly Closed','Connection Lost'] # Updated version without red. As we now your see connected and disconnected devices
path = "/tmp/clientState/"
sessionfile = "/share/.session/sessionfile"


class Tooltip:
    '''
    Found on https://stackoverflow.com/a/36221216
    The code of this class (up to line 183) is licensed under the terms of CC BY-SA 3.0: https://creativecommons.org/licenses/by-sa/3.0/
    Copyright 2016 crxguy52 et al.

    It creates a tooltip for a given widget as the mouse goes on it.

    see:

    http://stackoverflow.com/questions/3221956/
           what-is-the-simplest-way-to-make-tooltips-
           in-tkinter/36221216#36221216

    http://www.daniweb.com/programming/software-development/
           code/484591/a-tooltip-class-for-tkinter

    - Originally written by vegaseat on 2014.09.09.

    - Modified to include a delay time by Victor Zaccardo on 2016.03.25.

    - Modified
        - to correct extreme right and extreme bottom behavior,
        - to stay inside the screen whenever the tooltip might go out on
          the top but still the screen is higher than the tooltip,
        - to use the more flexible mouse positioning,
        - to add customizable background color, padding, waittime and
          wraplength on creation
      by Alberto Vassena on 2016.11.05.

      Tested on Ubuntu 16.04/16.10, running Python 3.5.2

    TODO: themes styles support
    '''

    def __init__(self, widget,
                 *,
                 bg='#FFFFEA',
                 pad=(5, 3, 5, 3),
                 text='widget info',
                 waittime=400,
                 wraplength=250):

        self.waittime = waittime  # in miliseconds, originally 500
        self.wraplength = wraplength  # in pixels, originally 180
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.onEnter)
        self.widget.bind("<Leave>", self.onLeave)
        #self.widget.bind("<ButtonPress>", self.onEnter)
        self.bg = bg
        self.pad = pad
        self.id = None
        self.tw = None

    def onEnter(self, event=None):
        self.schedule()

    def onLeave(self, event=None):
        self.unschedule()
        self.hide()

    def schedule(self):
        self.unschedule()
        self.id = self.widget.after(self.waittime, self.show)

    def unschedule(self):
        id_ = self.id
        self.id = None
        if id_:
            self.widget.after_cancel(id_)

    def show(self):
        def tip_pos_calculator(widget, label,
                               *,
                               tip_delta=(10, 5), pad=(5, 3, 5, 3)):

            w = widget

            s_width, s_height = w.winfo_screenwidth(), w.winfo_screenheight()

            width, height = (pad[0] + label.winfo_reqwidth() + pad[2],
                             pad[1] + label.winfo_reqheight() + pad[3])

            mouse_x, mouse_y = w.winfo_pointerxy()

            x1, y1 = mouse_x + tip_delta[0], mouse_y + tip_delta[1]
            x2, y2 = x1 + width, y1 + height

            x_delta = x2 - s_width
            if x_delta < 0:
                x_delta = 0
            y_delta = y2 - s_height
            if y_delta < 0:
                y_delta = 0

            offscreen = (x_delta, y_delta) != (0, 0)

            if offscreen:

                if x_delta:
                    x1 = mouse_x - tip_delta[0] - width

                if y_delta:
                    y1 = mouse_y - tip_delta[1] - height

            offscreen_again = y1 < 0  # out on the top

            if offscreen_again:
                # No further checks will be done.

                # TIP:
                # A further mod might automagically augment the
                # wraplength when the tooltip is too high to be
                # kept inside the screen.
                y1 = 0

            return x1, y1

        bg = self.bg
        pad = self.pad
        widget = self.widget

        # creates a toplevel window
        self.tw = tk.Toplevel(widget)

        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)

        win = tk.Frame(self.tw,
                       background=bg,
                       borderwidth=0)
        label = tk.Label(win,
                          text=self.text,
                          justify=tk.LEFT,
                          background=bg,
                          relief=tk.SOLID,
                          borderwidth=0,
                          wraplength=self.wraplength)

        label.grid(padx=(pad[0], pad[2]),
                   pady=(pad[1], pad[3]),
                   sticky=tk.NSEW)
        win.grid()

        x, y = tip_pos_calculator(widget, label)

        self.tw.wm_geometry("+%d+%d" % (x, y))

    def hide(self):
        tw = self.tw
        if tw:
            tw.destroy()
        self.tw = None




class Watchdog(PatternMatchingEventHandler, Observer):
    def __init__(self, path='.', patterns='*', updateClientfunc=print, renewClientListfunc=print):
        PatternMatchingEventHandler.__init__(self, patterns)
        Observer.__init__(self)
        self.schedule(self, path=path, recursive=False)
        self.updateClient = updateClientfunc
        self.renew = renewClientListfunc

    def on_modified(self, event):
        # update the specific client!
        try: 
            self.updateClient(event)
        except:
            self.renew()

class GUI:
    def __init__(self):
        try:
            os.chdir(path)
        except:
            #self.printErrorMsg()
            #os.chdir(path)
            pass
        
        self.watchdog = None
        self.watch_path = '.'
        self.root = Tk()
        self.root.title('z-Tree unleashed Control Center')

#        self.header = Frame(self.root, bg='grey', height=30)
        self.footer = Frame(self.root)

#        self.header.pack(fill='both')
        self.footer.pack(fill='both')

#        self.label_num_active = Label(
#            master=self.header, text=f"Number connected: ")
#        self.label_num_active.pack(side=RIGHT)

#        label4 = Label(master=self.footer, text=f"Disconnected",
#                       bg=a_colors[3], fg="white")
#        label4.pack(side=RIGHT)
        label3 = Label(master=self.footer, text='Inactive', # Third Label - Two States displayed in Tooltip
                       bg=a_colors[2], fg="white")
        label3.pack(side=RIGHT)
        label2 = Label(master=self.footer, text=states[1],  # Second Label
                       bg=a_colors[1], fg="white")
        label2.pack(side=RIGHT)
        label1 = Label(master=self.footer, text=states[0], # First Label never connected - blue
                       bg=a_colors[0], fg="white")
        label1.pack(side=RIGHT)
        
        self.start_watchdog()
        self.root.mainloop()       
    
    
    def printErrorMsg(self):
        try:
            self.error_msg.destroy()
        except:
            pass
        self.error_msg = Frame(self.root)
        self.error_msg.pack(fill='both')
        label_error_msg = Label(master=self.error_msg, text=f"Please restart the Control-Center!",
                       bg='yellow', fg="black",font=("Courier",16))
        label_error_msg.pack(side=RIGHT)
        
        
        
    def getFilesAndContents(self):  # Working: Read files and contents to a dict
        d = {}  # Create empty dictonary d
        try:
            for filename in os.listdir(path):
                with open(filename, 'r') as f:
                    filecontent = f.readline()
                    # Add to the dict d with key=filname - value = state
                    d[filename] = filecontent
        except:
            pass
        return d
        
        
    def getClientPortsAsKeyAndNameAsValue(self):
        d = {}  # Create empty dictonary d
        with open(sessionfile, 'r') as f:
            for line in f:
                fields = line.strip().split()
                # Add to the dict d with key=filname value = r
                d[str(fields[3])] = str(fields[0])
        return d

    
    def getSortedClientnameAndState(self): # replaces getClientPortsAsKeyAndNameAsValue
        state = self.getFilesAndContents() # get an dictionary which is indexable with strings
        client_list = []                   # Create empty list
        with open(sessionfile, 'r') as f:
            next(f) # Skip the first line
            for line in f:
                fields = line.strip().split()
                try:
                    client_list.append([str(fields[0]),int(state[str(fields[3])]),str(fields[3])])
                    #                      clientname  current_state[at_Portnumber]  port
                except:
                    self.printErrorMsg()
                    #pass # In case of a nonexisting key
        # Return as a list, as it is indexable with numbers 
        return client_list
        
    def initClientList(self):
        self.Port_to_names = self.getClientPortsAsKeyAndNameAsValue()
        self.clients = self.getSortedClientnameAndState()
#        num=str(sum([sublist.count(1) for sublist in self.clients]))
#        self.label_num_active['text'] = "Active clients: "+num
        used_clients = len(self.clients)
        # Let our grid be quadratic
        num_clients = math.ceil(math.sqrt(used_clients))
        content = Frame(self.root)
        
        client_k = 0
        for i in range(num_clients):
            content.columnconfigure(i, weight=1, minsize=75)
            content.rowconfigure(i, weight=1, minsize=50)
            for j in range(num_clients):
                if client_k < used_clients:  # Only add, if the number of clients is not exceeded. Remember, that we wanted to create a quadratic grid
                    frame = Frame(
                        master=content,
                        relief=RAISED,
                        name=self.clients[client_k][0],
                        bg=a_colors[self.clients[client_k][1]], # i.e. the state
                        borderwidth=1
                    )
                    
                    state_Code=int(self.clients[client_k][1])
                    last_update=time.ctime(os.path.getmtime(path+self.clients[client_k][2]))
                    Tooltip(frame, text='State: '+states[state_Code]+'\n\nLast Update: \n'+last_update, wraplength=200) # Update state
                    
                    frame.grid(row=i, column=j, padx=5, pady=5)
                    label = Label(
                        master=frame, text=f"{self.clients[client_k][0]}")
                    label.pack(padx=5, pady=5)
                    client_k = client_k+1
        return content

    def start_watchdog(self):
        self.content = self.initClientList()
        self.content.pack(fill='both', expand=True)
        if self.watchdog is None:
            self.watch_path = path
            self.watchdog = Watchdog(
                path=self.watch_path, updateClientfunc=self.updateClient,renewClientListfunc=self.renewClientList)
            self.watchdog.start()
        else:
            pass

    def stop_watchdog(self):
        if self.watchdog:
            self.watchdog.stop()
            self.watchdog = None
        else:
            pass

    def renewClientList(self): 
        self.content.destroy()
        self.content = self.initClientList()
        self.content.pack(fill='both', expand=True)

    def updateClient(self,event):
        # get filename i.e. the port of the changed file
        port = str(event.src_path[len(path):])
        clientname=self.Port_to_names[port]
        for frame in self.root.winfo_children()[2].winfo_children():
            if frame.winfo_name() == clientname: 
                # Open the state file of the calling participant
                with open(event.src_path, 'r') as f:
                    filecontent = f.readline()
                if len(filecontent) != 0: # In case the file is not written yet
                    state_Code=int(filecontent)
                    frame.configure(bg=a_colors[state_Code])
                    last_update=time.ctime(os.path.getmtime(event.src_path))
                    Tooltip(frame, text='State: '+states[state_Code]+'\n\nLast Update: \n'+last_update, wraplength=200) # Update state
                break

    
if __name__ == '__main__':
    gui=GUI() # Create a new instance in case of an exception and let the garbage collector clean up.

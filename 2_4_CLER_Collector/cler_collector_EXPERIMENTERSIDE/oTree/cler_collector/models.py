# Copyright 2020 Matthias L. Duch
#
# This software and associated documentation files can be freely redistributed
# under the terms of the MIT License, a copy of which you should have received.

from otree.api import (
    models,
    widgets,
    BaseConstants,
    BaseSubsession,
    BaseGroup,
    BasePlayer,
    Currency as c,
    currency_range,
)
import random
import string

def genpw(len = 16, letters = string.ascii_letters + string.digits):
    return ''.join(random.choices(letters, k = len))

def pwList(number_of_pws=1, pw_length=8):
    uniqueKeys=[str(genpw(pw_length)) for x in [*range(number_of_pws)]]
    i=1
    while len(uniqueKeys) != len(set(uniqueKeys)):
        print("Found duplicates while creating the list")
        print(i)
        uniqueKeys=[str(genpw(pw_length)) for x in [*range(number_of_pws)]]
        if i > 10:
            pw_length = pw_length + 1
        if i == 30:
            print("Error; Tried 30 times to draw a random sequence, but did not found one without duplicates... Try to increase pw_length... ")
            uniqueKeys="Error"
            break
        i += 1
    return uniqueKeys

	
doc = """
Der Link zur Datenspeicherung wird aus der Datei 
<br><br>
'cler_collector/targetWebsite.txt'
<br><br>
gelesen. Dort darf sich nur <b>eine</b> Zeile mit dem session-wide Link der Umfrage befinden. 
<br><br>
<b>Wichtig: </b><br>
Nachdem die Textdatei geändert wurde, muss der Prodserver neugestartet werden, damit die Datei von oTree gelesen wird!
<br><br>
Der folgende Link wurde aus der Datei 'cler_collector/targetWebsite.txt' gelesen.
Bitte vergleichen Sie den Link mit dem Ihnen zugesandten:
<br><br>
"""

class Constants(BaseConstants):
    name_in_url = 'cler_collector'
    players_per_group = None
    num_rounds = 1
    # Remember to restart the prodserver after changing this file!
    filepath='cler_collector/targetWebsite.txt'
    f = open(filepath,'r')
    url_of_the_email_survey=f.readline()
    f.close()
    
    global doc # Change the doc Information displayed on the oTree Admin page
    doc = doc + "<b>" + str(url_of_the_email_survey) + "</b>"
    # Der Link zur Datenspeicherung wird aus dieser Datei gelesen. Dazu darf sich nur eine Zeile in dieser Datei befinden.
    # Der Prodserver muss nach dem Einfügen oder Ändern des Links neugestartet werden!!!
    
    

class Subsession(BaseSubsession):
    def creating_session(self):
        n=len(self.get_players())
        unique_keys=pwList(n)
        i=0
        for p in self.get_players():
            p.uniqueID = unique_keys[i]
            #print(p.uniqueID)
            p.uniqueURL = Constants.url_of_the_email_survey + '?participant_label=' + p.uniqueID
            i=i+1
            

class Group(BaseGroup):
    pass


class Player(BasePlayer):
    uniqueID = models.StringField()
    uniqueURL = models.StringField()
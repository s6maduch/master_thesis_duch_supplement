# Copyright 2020 Matthias L. Duch
#
# This software and associated documentation files can be freely redistributed
# under the terms of the MIT License, a copy of which you should have received.

from otree.api import Currency as c, currency_range

from ._builtin import Page, WaitPage
from .models import Constants


class clerConsent(Page):
    form_model = 'player'
    #form_fields = [] # As we do not require any formfield to be entered. Maybe add a field which gets 1 when a participant clicked it.

page_sequence = [clerConsent]

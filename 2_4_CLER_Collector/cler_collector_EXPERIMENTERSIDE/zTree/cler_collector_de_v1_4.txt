treatment "cler_collector_de_v1_4_(ztree_4.1.7).ztt"{
	background{
		table globals{
		}
		table subjects{
		}
		table summary{
		}
		table contracts{
		}
		table session{
		}
		table logfile{
		}
		numsubjects = 1;
		numgroups = 1;
		numpracticeperiods = 0;
		numactualperiods = 1;
		exchangerate = 1;
		startendowment = 0;
		showupfee = 0;
		noAutoscope = FALSE;
		firstBoxesOnTop = FALSE;
		showupfeeawaytext = "Sie haben Verlust gemacht. Wollen Sie das Startgeld einsetzen, um diesen Verlust zu decken?";
		showupfeeawayyestext = "Ja";
		showupfeeawaynotext = "Nein";
		moneyawaytext = "Sie haben Verlust gemacht. Wollen Sie weiterfahren?";
		moneyawayyestext = "Ja";
		moneyawaynotext = "Nein";
		bancruptwaittext = "Bitte warten Sie bis Ihr Computer wieder freigegeben wird.";
		program{
			table = subjects;
			do{
				// Background - CLER_collect - PUT THIS PROGRAM INTO THE BACKGROUND AFTER LOGFILE
				// The Stage CLER_collect should be placed at the end of the experiment
				
				globals.do{
				  ds_timeout_first_page=5;
				  ds_timeout_last_page=10;
				}
				// Set time (in seconds) after which the first stage will be skipped. After this time, the subjects automatically skips all stages. The time starts when the program in Ztree is started
				
				
				
				
				
				
				
				// CLER_collect V. 1.3
				// Copyright 2020 Matthias L. Duch
				
				// This software and associated documentation files can be freely redistributed
				// under the terms of the MIT License, a copy of which you should have received.
				
				
				// AGAIN: THIS FILE NEEDS to be copied into the background.
				// Only then the code gets executed only once. 
				// Otherwise (e.g. if placed in a stage) the generated codes don't stay constant.
				
				// Initialize Variables to filter out subjects who did not show at all and those who finished the entire experiment
				// This will be set to 1 when subjects click on the very first button, thus making sure that they showed up
				active = 0;
				
				
				// This will be set to 1 when subjects click on the last button on the results screen which leads them to the code. If subjects don't click the button or when pushing subjects through the stages using "leave stage"; the code will not be displayed
				show_code = 0;
				
				// Correct until there are no duplicated randomnumbers anymore
				// You may change the border of the boxes
				ds_bgColorR=0;
				ds_bgColorG=0;
				ds_bgColorB=0;
				
				// Do not change the following lines...
				ds_duplicate=-99;
				
				while(subjects.sum(ds_duplicate)!=0){
				    subjects.do{
				        ds_rand=random(); 
				        ds_rand=rounddown(ds_rand*power(10,5),1); // round to integers
				
				        // prevent such numbers as 001111
				        ds_i=0;
				        while(ds_rand<1000 & ds_i < 30){
				                  ds_rand=random(); 
				                  ds_rand=rounddown(ds_rand*power(10,5),1); // round to integers
				                  ds_i=ds_i+1; // Prevent inifinte loops...
				        }
				
				        // Note: Commands are executed row by row. 
				        //Therefore duplicate just appears on the first found  duplication
				        ds_duplicate=if(ds_rand==subjects.find(not(same(Subject ))&same(ds_rand),ds_rand),1,0); 
				    }
				}
				
				// *Idea: Generate a random number from 1000-99,999 X_6
				// USE Luhns Algorithm as used for debit Card Number verification.
				// all digit at odd positions are doubled and if greater than 10 then 9 is substracted.
				// The check number is the sum of the doubled odd numbers (sometimes substracted by 9) 
				// and the even numbers. This number is multiplied by 9 and the rest of a integer division by 10.
				// The check is quite easy. Using this algorithm of doubled odd and even numbers the sums
				// a number where the integer division yields 0.
				
				subjects.do{
				    ds_varDigit1=rounddown(mod(ds_rand,100000)/10000,1);
				    ds_varDigit2=rounddown(mod(ds_rand,10000)/1000,1);
				    ds_varDigit3=rounddown(mod(ds_rand,1000)/100,1);
				    ds_varDigit4=rounddown(mod(ds_rand,100)/10,1);
				    ds_varDigit5=rounddown(mod(ds_rand,10)/1,1);
				
				// START Luhn's Algorithm
				    ds_varDigit6 = 0; // Initiaize the last digit.
				    if(ds_varDigit1*2>9){ds_varDigit6 = ds_varDigit6 + ds_varDigit1*2-9;}else{ds_varDigit6 = ds_varDigit6 + ds_varDigit1*2;};
				    ds_varDigit6 = ds_varDigit6 + ds_varDigit2;
				    if(ds_varDigit3*2>9){ds_varDigit6 = ds_varDigit6 + ds_varDigit3*2-9;}else{ds_varDigit6 = ds_varDigit6 + ds_varDigit3*2;};
				    ds_varDigit6 = ds_varDigit6 + ds_varDigit4;
				    if(ds_varDigit5*2>9){ds_varDigit6 = ds_varDigit6 + ds_varDigit5*2-9;}else{ds_varDigit6 = ds_varDigit6 + ds_varDigit5*2;};
				
				    ds_varDigit6 = round(mod((ds_varDigit6)*9,10),1); // Round to integers in case, there is a 0.999 or other...
				
				     // Additionally create the full number
				    ds_uniqueKey=ds_varDigit1*100000+ds_varDigit2*10000+ds_varDigit3*1000+ds_varDigit4*100+ds_varDigit5*10+ds_varDigit6;
				// END Luhn's Algorithm
				}
			}
		}
		screen action{
			usesbg = TRUE;
			withalertscreen = FALSE;
			noalertscreen = FALSE;
			headerbox "Header"{
				hasframe = TRUE;
				height = 10%;
				top = 0p;
				cuttop = TRUE;
				showPeriods = TRUE;
				showNumPeriods = TRUE;
				periodtext = "Periode";
				periodoftext = "von";
				practiceperiodprefix = "Probe ";
				showtime = TRUE;
				timestr = "Verbleibende Zeit [sec]:";
				pleasedecidetext = "Bitte entscheiden Sie sich jetzt!";
			}
		}
		screen wait{
			usesbg = TRUE;
			withalertscreen = FALSE;
			noalertscreen = FALSE;
		}
	}
	stage "Welcome Screen"{
		startwaitforall = FALSE;
		startConditionStr = "";
		singleentry = FALSE;
		singleentrycontinuation = FALSE;
		timeouttype = always;
		timeout = ds_timeout_first_page;
		screen action{
			usesbg = TRUE;
			withalertscreen = FALSE;
			noalertscreen = FALSE;
			containerbox "Container"{
				hasframe = TRUE;
				width = 90%;
				height = 90%;
				standardbox "Standard"{
					hasframe = FALSE;
					width = 60%;
					height = 70%;
					left = 20%;
					top = 10%;
					buttonposition = BOTTOMRIGHT;
					buttonsequence = HORIZONTAL;
					item{
						label = "<>{\\rtf \\fs32\\qc\\b Herzlich willkommen zu diesem Online-Experiment.}";
					}
					item{
					}
					item{
						label = "<>{\\rtf \\fs32\\qc\\b Klicken Sie bitte auf \"Weiter\", um mit dem Experiment zu beginnen.}";
					}
				}
				standardbox "Button"{
					hasframe = FALSE;
					height = 10%;
					right = 5%;
					bottom = 5%;
					buttonposition = BOTTOMRIGHT;
					buttonsequence = HORIZONTAL;
					button "Weiter"{
						clearinputafterok = FALSE;
						norecordmadeorselected = FALSE;
						terminatestage = TRUE;
						donotterminatestage = FALSE;
						specialbuttoncolor = TRUE;
						buttoncolor = 5921535;
						program{
							table = subjects;
							do{
								// Set active variable to 1, indicating that subject is active. Only when this variable is equal to 1 the stages of the experiment will be displayed.
								active  = 1;
							}
						}
					}
				}
			}
		}
		screen wait{
			usesbg = TRUE;
			withalertscreen = FALSE;
			noalertscreen = FALSE;
		}
	}
	stage "PLACE_YOUR_STAGES_HERE"{
		startwaitforall = FALSE;
		startConditionStr = "";
		singleentry = FALSE;
		singleentrycontinuation = FALSE;
		timeouttype = always;
		timeout = 10;
		program{
			table = subjects;
			do{
				// Paste this program to all your Stages. Then participants only enter when they 
				// have clicked on the enter button in time.
				Participate=if(active==1,1,0);
				
			}
		}
		screen action{
			usesbg = TRUE;
			withalertscreen = FALSE;
			noalertscreen = FALSE;
			standardbox "Standard"{
				hasframe = TRUE;
				buttonposition = BOTTOMRIGHT;
				buttonsequence = HORIZONTAL;
				item{
					label = "Delete this stage and place your stages here!!!";
				}
			}
		}
		screen wait{
			usesbg = TRUE;
			withalertscreen = FALSE;
			noalertscreen = FALSE;
		}
	}
	stage "End_cler_collector"{
		startwaitforall = FALSE;
		startConditionStr = "";
		singleentry = FALSE;
		singleentrycontinuation = FALSE;
		timeouttype = always;
		timeout = ds_timeout_last_page;
		screen action{
			usesbg = FALSE;
			withalertscreen = FALSE;
			noalertscreen = FALSE;
			containerbox "Press to display code"{
				hasframe = TRUE;
				width = 90%;
				height = 90%;
				displaycondition = active==1;
				standardbox "Standard"{
					hasframe = FALSE;
					width = 60%;
					height = 20%;
					top = 60%;
					buttonposition = BOTTOMRIGHT;
					buttonsequence = HORIZONTAL;
					item{
						label = "<>{\\rtf\\fs28\\qc\\b WICHTIG: Um Ihre Auszahlung zu erhalten, ben�tigen Sie einen Code, welchen Sie zusammen mit Ihrer bei PayPal hinterlegten E-Mail-Adresse in ein Formular eingeben m�ssen. Sie gelangen zu dem Formular, indem Sie auf den zweiten Link in der Einladungs-E-Mail zu diesem Experiment klicken. Der Code wird Ihnen auf der n�chsten Seite angezeigt.";
					}
				}
				standardbox "Button"{
					hasframe = FALSE;
					height = 10%;
					right = 5%;
					bottom = 5%;
					buttonposition = BOTTOMRIGHT;
					buttonsequence = HORIZONTAL;
					button "   Zum Code   "{
						clearinputafterok = TRUE;
						norecordmadeorselected = FALSE;
						terminatestage = TRUE;
						donotterminatestage = FALSE;
						specialbuttoncolor = TRUE;
						buttoncolor = 5921535;
						program{
							table = subjects;
							do{
								// Set variable to 1 for those who finished the experiment. Only for those the code will be displayed.
								show_code = 1;
							}
						}
					}
				}
			}
			containerbox "No Show"{
				hasframe = TRUE;
				width = 90%;
				height = 90%;
				displaycondition = active==0;
				standardbox "Standard"{
					hasframe = FALSE;
					width = 60%;
					height = 20%;
					left = 20%;
					top = 40%;
					buttonposition = BOTTOMRIGHT;
					buttonsequence = HORIZONTAL;
					item{
						label = "{\\rtf\\fs32\\qc\\b Es ist keine Teilnahme mehr m�glich. Vielen Dank f�r Ihr Interesse.";
					}
				}
			}
		}
		screen wait{
			usesbg = FALSE;
			withalertscreen = FALSE;
			noalertscreen = FALSE;
			containerbox "Show Code"{
				hasframe = TRUE;
				displaycondition = show_code==1&active==1;
				containerbox "ds_container"{
					hasframe = FALSE;
					width = 560p;
					containerbox "Title_Container"{
						hasframe = TRUE;
						width = 564p;
						height = 120p;
						top = 20%;
						cuttop = TRUE;
						standardbox "Standard"{
							hasframe = TRUE;
							buttonposition = BOTTOMRIGHT;
							buttonsequence = HORIZONTAL;
							item{
								label = "Bitte notieren Sie sich den folgenden Code.";
							}
							item{
								label = "Rufen Sie bitte jetzt den zweiten Link auf, den Sie mit der Einladungmail ";
							}
							item{
								label = "zu diesem Experiment erhalten haben und geben dort den notierten Code ein.";
							}
						}
					}
					containerbox "ds_DisplayDigits"{
						hasframe = FALSE;
						width = 564p;
						height = 119p;
						left = 3p;
						top = 50p;
						cuttop = TRUE;
						containerbox "FirstDigit"{
							hasframe = TRUE;
							width = 84p;
							height = 119p;
							left = 0p;
							plotbox "OuterFrame"{
								hasframe = TRUE;
								bgcolor = rgb(ds_bgColorR,ds_bgColorG,ds_bgColorB);
								maintainaspectratio = FALSE;
								xaxistype = LINEAR;
								yaxistype = LINEAR;
								xleft = -100;
								xright = 100;
								ybottom = -100;
								ytop = 100;
							}
							plotbox "InnerFrame"{
								hasframe = TRUE;
								left = 5p;
								right = 5p;
								top = 5p;
								bottom = 5p;
								maintainaspectratio = FALSE;
								xaxistype = LINEAR;
								yaxistype = LINEAR;
								xleft = -100;
								xright = 100;
								ybottom = -100;
								ytop = 100;
								plottext "Digit1"{
									text = "<> <ds_varDigit1|1> ";
									x = 0;
									y = -40;
									horizontalalignment = CENTER;
									verticalalignment = CENTER;
									textcolor = rgb(0,0,0);
									fontsize = 90;
								}
							}
						}
						containerbox "SecondDigit"{
							hasframe = TRUE;
							width = 84p;
							height = 119p;
							left = 94p;
							plotbox "OuterFrame"{
								hasframe = TRUE;
								bgcolor = rgb(ds_bgColorR,ds_bgColorG,ds_bgColorB);
								maintainaspectratio = FALSE;
								xaxistype = LINEAR;
								yaxistype = LINEAR;
								xleft = -100;
								xright = 100;
								ybottom = -100;
								ytop = 100;
							}
							plotbox "InnerFrame"{
								hasframe = TRUE;
								left = 5p;
								right = 5p;
								top = 5p;
								bottom = 5p;
								maintainaspectratio = FALSE;
								xaxistype = LINEAR;
								yaxistype = LINEAR;
								xleft = -100;
								xright = 100;
								ybottom = -100;
								ytop = 100;
								plottext "Digit2"{
									text = "<> <ds_varDigit2|1> ";
									x = 0;
									y = -40;
									horizontalalignment = CENTER;
									verticalalignment = CENTER;
									textcolor = rgb(0,0,0);
									fontsize = 90;
								}
							}
						}
						containerbox "ThirdDigit"{
							hasframe = TRUE;
							width = 84p;
							height = 119p;
							left = 188p;
							plotbox "OuterFrame"{
								hasframe = TRUE;
								bgcolor = rgb(ds_bgColorR,ds_bgColorG,ds_bgColorB);
								maintainaspectratio = FALSE;
								xaxistype = LINEAR;
								yaxistype = LINEAR;
								xleft = -100;
								xright = 100;
								ybottom = -100;
								ytop = 100;
							}
							plotbox "InnerFrame"{
								hasframe = TRUE;
								left = 5p;
								right = 5p;
								top = 5p;
								bottom = 5p;
								maintainaspectratio = FALSE;
								xaxistype = LINEAR;
								yaxistype = LINEAR;
								xleft = -100;
								xright = 100;
								ybottom = -100;
								ytop = 100;
								plottext "Digit3"{
									text = "<> <ds_varDigit3|1> ";
									x = 0;
									y = -40;
									horizontalalignment = CENTER;
									verticalalignment = CENTER;
									textcolor = rgb(0,0,0);
									fontsize = 90;
								}
							}
						}
						containerbox "FourthDigit"{
							hasframe = TRUE;
							width = 84p;
							height = 119p;
							left = 282p;
							plotbox "OuterFrame"{
								hasframe = TRUE;
								bgcolor = rgb(ds_bgColorR,ds_bgColorG,ds_bgColorB);
								maintainaspectratio = FALSE;
								xaxistype = LINEAR;
								yaxistype = LINEAR;
								xleft = -100;
								xright = 100;
								ybottom = -100;
								ytop = 100;
							}
							plotbox "InnerFrame"{
								hasframe = TRUE;
								left = 5p;
								right = 5p;
								top = 5p;
								bottom = 5p;
								maintainaspectratio = FALSE;
								xaxistype = LINEAR;
								yaxistype = LINEAR;
								xleft = -100;
								xright = 100;
								ybottom = -100;
								ytop = 100;
								plottext "Digit4"{
									text = "<> <ds_varDigit4|1> ";
									x = 0;
									y = -40;
									horizontalalignment = CENTER;
									verticalalignment = CENTER;
									textcolor = rgb(0,0,0);
									fontsize = 90;
								}
							}
						}
						containerbox "FifthDigit"{
							hasframe = TRUE;
							width = 84p;
							height = 119p;
							left = 376p;
							plotbox "OuterFrame"{
								hasframe = TRUE;
								bgcolor = rgb(ds_bgColorR,ds_bgColorG,ds_bgColorB);
								maintainaspectratio = FALSE;
								xaxistype = LINEAR;
								yaxistype = LINEAR;
								xleft = -100;
								xright = 100;
								ybottom = -100;
								ytop = 100;
							}
							plotbox "InnerFrame"{
								hasframe = TRUE;
								left = 5p;
								right = 5p;
								top = 5p;
								bottom = 5p;
								maintainaspectratio = FALSE;
								xaxistype = LINEAR;
								yaxistype = LINEAR;
								xleft = -100;
								xright = 100;
								ybottom = -100;
								ytop = 100;
								plottext "Digit5"{
									text = "<> <ds_varDigit5|1> ";
									x = 0;
									y = -40;
									horizontalalignment = CENTER;
									verticalalignment = CENTER;
									textcolor = rgb(0,0,0);
									fontsize = 90;
								}
							}
						}
						containerbox "SixthDigit"{
							hasframe = TRUE;
							width = 84p;
							height = 119p;
							left = 470p;
							plotbox "OuterFrame"{
								hasframe = TRUE;
								bgcolor = rgb(ds_bgColorR,ds_bgColorG,ds_bgColorB);
								maintainaspectratio = FALSE;
								xaxistype = LINEAR;
								yaxistype = LINEAR;
								xleft = -100;
								xright = 100;
								ybottom = -100;
								ytop = 100;
							}
							plotbox "InnerFrame"{
								hasframe = TRUE;
								left = 5p;
								right = 5p;
								top = 5p;
								bottom = 5p;
								maintainaspectratio = FALSE;
								xaxistype = LINEAR;
								yaxistype = LINEAR;
								xleft = -100;
								xright = 100;
								ybottom = -100;
								ytop = 100;
								plottext "Digit6"{
									text = "<> <ds_varDigit6|1> ";
									x = 0;
									y = -40;
									horizontalalignment = CENTER;
									verticalalignment = CENTER;
									textcolor = rgb(0,0,0);
									fontsize = 90;
								}
							}
						}
					}
					containerbox "Subtitle_Container"{
						hasframe = TRUE;
						width = 564p;
						height = 80p;
						top = 50p;
						plotbox "Subtitle"{
							hasframe = TRUE;
							maintainaspectratio = FALSE;
							xaxistype = LINEAR;
							yaxistype = LINEAR;
							xleft = -100;
							xright = 100;
							ybottom = -100;
							ytop = 100;
							plottext "Text"{
								text = "Bitte bewahren Sie diese Ziffernfolge auf.";
								horizontalalignment = CENTER;
								verticalalignment = CENTER;
								textcolor = rgb(0,0,0);
								bold = TRUE;
								fontsize = 14;
							}
						}
					}
				}
			}
			containerbox "No Show"{
				hasframe = TRUE;
				width = 90%;
				height = 90%;
				displaycondition = active==0|show_code==0;
				standardbox "Standard"{
					hasframe = FALSE;
					width = 60%;
					height = 20%;
					left = 20%;
					top = 40%;
					buttonposition = BOTTOMRIGHT;
					buttonsequence = HORIZONTAL;
					item{
						label = "{\\rtf\\fs32\\qc\\b Es ist keine Teilnahme mehr m�glich. Vielen Dank f�r Ihr Interesse..";
					}
				}
			}
		}
	}
	roles{
		role "S 1"{
		}
	}
	period "1"{
		prompt = "";
		subject 1{
			name = "";
			group = 1;
		}
	}
}
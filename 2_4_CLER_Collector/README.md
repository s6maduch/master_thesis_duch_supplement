# cler_collector

## Purpose
`cler_collector` ensures that the decisions made in an experiment are technically and organizationally separated from personal information.
We distinguish between the experiment side (i.e. the conducted experiment / survey) and the more static server side (i.e where the participants 
are forwarded to and enter the payment information) which could be offered by your local laboratory such that the two parts, 
the experimental decisions and the payment information, are stored separately.

## How does it work?

`cler_collector` is a stand alone oTree[^1] (or LimeSurvey) app that provides a form to store participants' personal information (e.g. PayPal-address or bank account) together 
with a unique random identifier (URI) in a separate data base. In a first step a URI is generated and is displayed on the participants' screen at the end of an experiment. 
Participants can write it down or copy it to the clipboard of the their device before following a link to the `cler_collector` form. In a second step participants are asked to
enter their personal URI and the information that is necesarry for the payment procedure. The form also asks for particpants' consent to a privacy policy. 
This privacy policy can be adjusted in the corresponding templates.  

# Experimenter Side

This section describes how to forward participants from your conducted experiment to 
the page where they have to enter their personal information in the most simple way. 

## Use cases

### (I) otree to cler_connector

The `cler_collector` can be included in oTree experiments, as an oTree app, which creates unique keys and embeds them in a button. 
Once this button is pressed participants are redirected to cler_collector (the *proper*unique key is submitted automatically).

### (II) zTree to cler_connector**

The `cler_collector` can also be connected to z-Tree experiments. We created a stage, that provides a simple checksum algorithm and creates random 
numbers which are checked for consistency once entered on the cler_collector form. Once the experiment has concluded, the experimenter creates a table containing 
only the payment variable and the unique key. This table can then be merged with the data collected by cler_collector's server side app. 
The person who mergee the two tables should be an independent third party (e.g. lab manager). The result is a table containing the payment information and the amount to pay out.
Ideally, no single person has access to both behavioral and payment data.

### (III) Survey to cler_connector**

Of course, the `cler_collector`can receive a URI from any online survey platform as well. As long as the sending platforms can generate URIs and append them to a link, 
the `cler_connector` can capture the appended value and store it in the ```participant_label``` variable. See the instructions section for an example using the *Qualtrics* platform. 



## How to use `cler_connector` 
*Part I*
The next steps depend on the use case you want to implement:

### (I) otree to cler_connector

1. Download the latest version of `cler_collector_EXPERIMENTERSIDE` app from this repository.
2. Unzip the archive
3. Copy the folder `cler_collector` into your project folder. (Note that the folder name is *cler_collector* and not *cler_collector_EXPERIMENTERSIDE*) 
4. Append `'cler_collector'` at the end the `app_sequence` of your project.

**Example:**
```
SESSION_CONFIGS = [
    dict(
        name='your_App',
        display_name="your App",
        num_demo_participants=3,
        app_sequence=['your_App_Part1','your_App_Part2'*'cler_collector'],
    ),
...
```

5. Navigate to the cler_collector folder and open the file `targetWebsite.txt`
6. Copy the session wide link from the `cler_collector_SERVERSIDE` session you created in *Part II*
7. (Re)start your oTree Server. **Note**: The `cler_collector` app should run a different oTree Server than the `cler_collector_SERVERSIDE` app. Otherwise all data collected with your experiment's app(s) and in the `cler_collector_SERVERSIDE` app will be exported into a single file. This would obviously undermine the whole purpose of the `cler_collector`.   
8. Âdjust the texts, links and logos to the specifics of your laboratory.

### (II) zTree to cler_connector

1. Import `cler_collector_EXPERIMENTERSIDE/cler_collector_XX_v1_4.txt` with z-Tree[^2].
2. Copy the background programs to the background of your z-Tree program
3. Copy the stage to the end of your z-Tree program
4. Make sure to tick `use_checksum` during the creation of a new session in the oTree[^1] backend (see *Part 1*). Then users will be notified in case their entered key appears to be invalid.

![Use the built in zTree checksum validation in our oTree App.](cler_collector_EXPERIMENTERSIDE/tick_use_checksum_if_you_use_zTree.png)

*z-Tree compatibility and supported screen resolution*

- Tested and working with z-Tree Version 3.6.7 and upwards up to the latest version (4.1.11) and probably beyond.
- Displays very well on each resolution starting at 800x600.

Using *cler_collector* along [z-Tree unleashed](https://cler1.gitlab.io/ztree-unleashed-doc/docs/home/) makes payments as simple as possible.

### (III) Qualtrics (or any other survey platform) to cler_connector**

#### Using our Serverside oTree App (`cler_collector_SERVERSIDE`)

1. Navigate to your *Messages Library*
2. Add a new *End of Survey Message* or edit an existing one
3. Insert a link and Copy the session wide link from the `cler_collector_SERVERSIDE` session you created in *Part II* into the URL field. (see screenshot below)
4. Attach the following code (!no spaces) to the session wide link `?participant_label=${e://Field/ResponseID?format=urlencode}` 

![add the link to your cler_collector_SERVERSIDE session](cler_collector_EXPERIMENTERSIDE/qualtrics_cler_collector.png)

5. Add an *End of Survey Element* at the end of your survey flow (or edit an exisiting one).
2. Click the customize option, select *Custom end of survey message...* and select the message you just created (or modified).

![add your custom message to the end of survey](cler_collector_EXPERIMENTERSIDE/qualtrics_cler_collector_end_of_survey.png)

See `https://www.qualtrics.com/support/survey-platform/survey-module/survey-flow/standard-elements/passing-information-through-query-strings/` for more information.


#### **NEW** Using our Serverside LimeSurvey Survey

1. Navigate to your *Messages Library*
2. Add a new *End of Survey Message* or edit an existing one
3. Insert the link to the Serverside LimeSurvey.
4. Attach the following code (!no spaces) to the session wide link `?PaymentCode=${e://Field/ResponseID?format=urlencode}` 

![add the link to your cler_collector_SERVERSIDE session](cler_collector_EXPERIMENTERSIDE/qualtrics_cler_collector_to_LSurvey.png)

5. Add an *End of Survey Element* at the end of your survey flow (or edit an exisiting one).
2. Click the customize option, select *Custom end of survey message...* and select the message you just created (or modified).

![add your custom message to the end of survey](cler_collector_EXPERIMENTERSIDE/qualtrics_cler_collector_end_of_survey.png)

See `https://www.qualtrics.com/support/survey-platform/survey-module/survey-flow/standard-elements/passing-information-through-query-strings/` for more information.



# Setting up the Serverside

The serverside part is responsible for capturing the personal information of
the participants. Furthermore we use HTTP-Methods like GET in order to receive 
the unique key generated by oTree or Qualtrics and obviate the need for
participants to enter this key by hand.


## Using our oTree Serverside App

*Part II*

1. Download the latest version of `cler_collector_SERVERSIDE` app from this repository.
2. Unzip the archive
3. Copy the folder `cler_collector_SERVERSIDE` into your otree project folder.
4. Add the `cler_collector_SERVERSIDE` app to your `SESSION_CONFIGS` in the `settings.py`in your project

**Example:**
```
SESSION_CONFIGS = [
    dict(
        name='cler_collector_SERVERSIDE',
        display_name="cler_collector_SERVERSIDE",
        num_demo_participants=100,
        app_sequence=['cler_collector_SERVERSIDE'],
    ),
...
```
5. Start your oTree server 
6. Create a new session for the `cler_collector_SERVERSIDE` app with a sufficient number of participants

## Using our LimeSurvey Serverside App

1. Import into LimeSurvey.
2. Activate survey.
3. The payment code can be pre-entered by appending `?PaymentCode=[CODE]` (or `&PaymentCode=[CODE]`) to the survey's URL.

## License

*cler_collector* is licensed under the MIT License. Please read `LICENSE` for more information.

## Implementation Information

Non-server side code for collecting information is reduced to a bare minimum. Relying on Client-Side executed code like JavaScript
would be useless in case a participant deactivates it in her browser (most likely this would lead to issues in
the experiment itself, but it will not affect collecting the personal information). Therefore each record is
transferred securely (only if a proper SSL/TLS certificate is installed!) via HTTPS and checked with server
side python code and stored in the data base only if the consent form is checked.

## FAQ

**Do I have to cite anything if I use this program?**

No! But remember to cite z-Tree[^2], oTree[^1] or z-Tree unleashed[^3] in case you use them for your research.

## References

[^1]: Chen, D. L., Schonger, M., and Wickens, C. (2016). "oTree — An open-source platform for laboratory, online, and field experiments". Journal of Behavioral and Experimental Finance, 9, pp. 88–97.
[^2]: Fischbacher, U. (2007). "z-Tree: Zurich toolbox for ready-made economic experiments". Experimental economics, 10(2), pp. 171–178.
[^3]: Duch, Matthias & Grossmann, Max & Lauer, Thomas. (2020). "z-Tree unleashed: A novel client-integrating architecture for conducting z-Tree experiments over the Internet." 

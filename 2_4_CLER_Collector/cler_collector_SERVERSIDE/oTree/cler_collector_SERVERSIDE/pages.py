# Copyright 2020 Matthias L. Duch
#
# This software and associated documentation files can be freely redistributed
# under the terms of the MIT License, a copy of which you should have received.

from otree.api import Currency as c, currency_range

from ._builtin import Page, WaitPage
from .models import Constants
    
class clercollect(Page):
    form_model = 'player'
    form_fields = ['agreedConsent','enterUniqueCode','email']
    
    # Receive the participant label as part of the URL
    def vars_for_template(self):
        if(self.participant.label is not None):
            received_label=True
            self.player.receivedUniqueID=self.participant.label    # Save it as a variable in the 'visible' table
            self.player.enterUniqueCode="GotCodeByURL"             # Save, that a GET content was submitted (i.e. ?participant_label= was appended) an no code has to be entered
        else:
            received_label=False
            self.player.receivedUniqueID="CodeNotSubmittedByURL"   # Save, that no GET content was submitted (i.e. ?participant_label was not appended)

        return dict(
            received_label=received_label,
			use_checksum=self.subsession.session.config['use_checksum'],
        ) 

        
class lastpage(Page):
    pass

page_sequence = [clercollect,lastpage]

# Copyright 2020 Matthias L. Duch
#
# This software and associated documentation files can be freely redistributed
# under the terms of the MIT License, a copy of which you should have received.

from otree.api import (
    models,
    widgets,
    BaseConstants,
    BaseSubsession,
    BaseGroup,
    BasePlayer,
    Currency as c,
    currency_range,
)
from django.utils.safestring import mark_safe


class Constants(BaseConstants):
    name_in_url = 'cler_collector_SERVERSIDE'
    players_per_group = None
    num_rounds = 1
    privacy_label_text='''
Beispieltext:<br>
Hiermit stimme ich den <a href="https://EIGENEWEBSITEHIEREINTRAGEN" target="_blank">Datenschutzbestimmungen des 

<i>Hier Name des Labors eingeben (\cler_collector_SERVERSIDE\models.py)</i>
</a> zu.<b><br>
Darüber hinaus stimme ich zu, dass meine Verhaltensdaten mit den in diesem Formular angegebenen personenbezogenen 
Daten für die Durchführung der Auszahlung verknüpft werden können.</b><br>
Nach dem Abschluss der Auszahlung werden meine personenbezogenen Daten aus den Datenverarbeitungssystemen des 

<i>Hier Name des Labors eingeben (\cler_collector_SERVERSIDE\models.py)</i>
entfernt; meine anonymisierten Verhaltensdaten sowie Aufbewahrungsfristen 
in den Datenverarbeitungssystemen Dritter (z.B. Zahlungsdienstleister) und für die Kassenprüfung in 
Übereinstimmung mit den Gesetzen des Landes 

<i>Hier Name eingeben (\cler_collector_SERVERSIDE\models.py)</i>
sind davon unberührt.	
'''


class Subsession(BaseSubsession):
    pass
            

class Group(BaseGroup):
    pass
    

class Player(BasePlayer):
    receivedUniqueID = models.StringField(label='Received Code in URL')
   
    agreedConsent = models.BooleanField(
        widget=widgets.CheckboxInput(),
        label=Constants.privacy_label_text,
    )
    def agreedConsent_error_message(self, values):
        if (values == False): # Check whether the checkbox is checked.
            self.participant.vars['var_consent'] = False # Use this as a simple variable to store the value of the checkbox. This is a python variable and not stored in the database. Accessing self.agreedConsent pointed out to be too slow to be used in this context of checking whether the box was checked and only then storing the mailaddress. In any other case the mail address would be written to the database, even in case one would not hat agreed to do so.
            return 'Bitte bestätigen Sie unsere Datenschutzbestimmungen.'
        else:            
            self.participant.vars['var_consent'] = True

    enterUniqueCode = models.StringField(blank=True,label='Bitte geben Sie hier Ihren Code ein:') # In case someone can't use GET forwardings, we need some unique key. I.e. for the use in Qualtriqs
    def enterUniqueCode_error_message(self, values):
        if (self.participant.label is None) and (values is None):
            return 'Bitte geben Sie den Ihnen mitgeteilten Code ein.'
		
        # This command prevents that the unique code gets written to the database without having accepted the privacy policy     
        # It is a fallback in case the required flag in the clientside html input is not working.
        if self.participant.vars['var_consent'] == False:  # Issue with self.agreedConsent, somehow it takes longer to write the value to the database and read it, so this check did not work before. self.participant.vars['var_consent'] is just a variable.
            return ' ' # Empty string necessary in order not to record a subject who did not give his consent.

    email = models.StringField(label='Bitte geben Sie Ihre E-Mail-Adresse ein:')
    def email_error_message(self, values):
        import re
        # Regex created by https://emailregex.com/
        # If no mail is entered or if it is not matched as an email
        p = re.compile('''(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])''')
        
        # Return if a mail address is submitted which does not match
        if (values is None) or (not p.match(values)):
            return 'Bitte geben Sie eine korrekte E-Mail Adresse ein (Beachten Sie, dass die E-Mail Adresse kleingeschrieben werden muss).'
        
        # In case a correct mail is submitted the checkbox gets checked again.
        # This command prevents that the mail address gets written to the database without having accepted our Privacy policy     
        # This is a fallback in case the required flag in the clientside html input is not working.
        if self.participant.vars['var_consent'] == False:  # Issue with self.agreedConsent, somehow it takes longer to write the value to the database and read it, so this check did not work before. self.participant.vars['var_consent'] is just a variable.
            return ' ' # Empty string necessary in order not to record a subject who did not give his consent.

unit Unit1;
// Developed by Matthias Duch, 2020
// cler_leaf_launcherTabszTu
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  ComCtrls, Menus, Process, Math,LCLIntf,LCLType,LazFileUtils {needed for GetShellLinkTarget},winpeimagereader  {need this for reading exe info}, FileUtil, fileinfo;

type

  { Ttitle_cler_z_leaf_launcher }

  Ttitle_cler_z_leaf_launcher = class(TForm)
    Button1: TButton;
    Button2: TButton;
    labelfoundVers: TLabel;
    versLabel: TLabel;
    ReplaceTextButton: TButton;
    searchForString: TEdit;
    replaceStringWithThis: TEdit;
    GroupBox2: TGroupBox;
    startzLeafsInBrowser: TButton;
    GetBrowserDir: TButton;
    pathBrowserExe: TEdit;
    GroupBox1: TGroupBox;
    inputfieldLinks: TMemo;
    RadioButton1: TRadioButton;
    RadioButton4: TRadioButton;
    zTuGroupBox: TGroupBox;
    SelectzLeafDirButton: TButton;
    basicSettingsGroup: TGroupBox;
    advancedSettingsGroup: TGroupBox;
    startLeafs: TButton;
    PageControl2: TPageControl;
    decreaseLeafs: TButton;
    checkBoxChangedPath: TCheckBox;
    inputChangedPath: TEdit;
    labelChangeDir: TLabel;
    GetzLeafDir: TOpenDialog;
    stopLeafs: TButton;
    inputAdditionalParameters: TEdit;
    labelFullscreen: TLabel;
    labelCascade: TLabel;
    labelAdditionalParameters: TLabel;
    inputPrefix: TEdit;
    inputStartWithNumber: TEdit;
    inputNumLeafs: TEdit;
    inputFontsize: TEdit;
    setWidth: TEdit;
    setHeight: TEdit;
    setResolution: TLabel;
    setDelimiter: TLabel;
    labelNumLeafs: TLabel;
    labelStartWithNumber: TLabel;
    labelPrefix: TLabel;
    labelFontsize: TLabel;
    checkboxSwitchEntryResolution: TCheckBox;
    checkboxCascade: TCheckBox;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    procedure labelfoundVersClick(Sender: TObject);
    procedure ReplaceTextButtonClick(Sender: TObject);
    procedure FormDropFiles(Sender: TObject; const FileNames: array of String);
    procedure GetBrowserDirClick(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure GroupBox2Click(Sender: TObject);
    procedure inputfieldLinksChange(Sender: TObject);
    procedure inputfieldLinksDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure PageControl2Change(Sender: TObject);
    procedure pathBrowserExeChange(Sender: TObject);
    procedure RadioButton2Change(Sender: TObject);
    procedure RadioButton3Change(Sender: TObject);
    procedure RadioButton4Change(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure SelectzLeafDirButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure decreaseLeafsClick(Sender: TObject);
    procedure checkBoxChangedPathChange(Sender: TObject);
    procedure inputChangedPathChange(Sender: TObject);
    procedure inputNumLeafsExit(Sender: TObject);
    procedure inputStartWithNumberExit(Sender: TObject);
    procedure setWidthChange(Sender: TObject);
    procedure startLeafsClick(Sender: TObject);
    procedure startzLeafsInBrowserClick(Sender: TObject);
    procedure stopLeafsClick(Sender: TObject);
    procedure checkboxSwitchEntryResolutionChange(Sender: TObject);
    procedure inputPrefixChange(Sender: TObject);
    procedure GroupBox1Click(Sender: TObject);
    procedure loadConfigClick(Sender: TObject);
    procedure labelStartWithNumberClick(Sender: TObject);
    procedure labelPrefixClick(Sender: TObject);
    procedure labelFontsizeClick(Sender: TObject);
    procedure labelNumLeafsClick(Sender: TObject);
    procedure launchProcess(param: String; errormsg: String);
    procedure startBrowser(param:String; errormsg:String);
    procedure readLinkList(path:String);
    procedure getzLeafVersion();
    procedure versLabelClick(Sender: TObject);
    procedure writeConfig();
    procedure readConfig();
  private

  public

  end;

var
  title_cler_z_leaf_launcher: Ttitle_cler_z_leaf_launcher;
  oldinputNumLeafs: Integer = 0; // Check to prevent duplicated z-Leafs which causes z-Tree to crash
  oldinputPrefix: String = '';
  globalErrorOpenzLeaf: Integer = 0;

implementation

{$R *.lfm}
uses Unit2;


{ Ttitle_cler_z_leaf_launcher }


procedure Ttitle_cler_z_leaf_launcher.launchProcess(param: String; errormsg: String);
var
  Process: TProcess;
  I: Integer;
begin
  Process := TProcess.Create(nil);
  if(globalErrorOpenzLeaf=0) then
  begin
    try
      try
      Process.InheritHandles := False;
      Process.Options := [];
      Process.ShowWindow := swoShow;

      // Copy default environment variables including DISPLAY variable for GUI application to work
      for I := 1 to GetEnvironmentVariableCount do
        Process.Environment.Add(GetEnvironmentString(I));

      Process.Executable := param;
      Process.Execute;

      finally
         begin
           Process.Free;
           Sleep(250); // Wait for a short period such that the programs start sequentially.
         end;
      end;
    except
      begin
         showMessage(errormsg);
         globalErrorOpenzLeaf:=1; // Prevent the launcher from trying this multiple times...
      end;
    end;
  end;
end;

procedure Ttitle_cler_z_leaf_launcher.FormCreate(Sender: TObject);
begin
  //ShowMessage(IntToStr(PageControl2.ActivePage.TabIndex));
  //ShowMessage(ParamStr(1));
  readConfig();
  if (ParamCount>0) then      // Show potential params i.e. an exe dragged on the launchers' exe
    begin
        PageControl2.ActivePageIndex:=0;   // Go to page 1 - z-Tree
        checkBoxChangedPath.checked:=False; // Show this input field
        inputChangedPath.Text:=ParamStr(1); // Overwrite the path in the input field
        ShowMessage('z-Leaf path set to: '+ParamStr(1));
        // Set path to the path, where the exe is located..
        SetCurrentDir(ExtractFilePath(ParamStr(0)));
        ShowMessage(GetCurrentDir);
    end;
    getzLeafVersion();
end;

procedure Ttitle_cler_z_leaf_launcher.getzLeafVersion();
var
  FileVerInfo:TFileVersionInfo;
  commandstring:String;
begin

    if checkBoxChangedPath.checked=True then   // use exe in curr dir
       commandstring:=Concat(GetCurrentDir,'\zleaf.exe')
    else
       commandstring:=inputChangedPath.Text; // use the inputfield

  try
    try
      FileVerInfo:=TFileVersionInfo.Create(nil);
      FileVerInfo.FileName:= commandstring;
      FileVerInfo.ReadFileInfo;
      labelfoundVers.Caption:=FileVerInfo.VersionStrings.Values['FileDescription'];
    finally
      FileVerInfo.Free;
    end;
  except
      begin
         labelfoundVers.Caption:='none';
      end;
  end;
   //https://spring4d.4delphi.com/docs/master/Html/index.htm?Spring.Utils.TFileVersionInfo.htm
   // https://forum.lazarus.freepascal.org/index.php?topic=41862.0
   //https://wiki.freepascal.org/Show_Application_Title,_Version,_and_Company
end;

procedure Ttitle_cler_z_leaf_launcher.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  writeConfig();
end;

procedure Ttitle_cler_z_leaf_launcher.SelectzLeafDirButtonClick(Sender: TObject);
var filename : string;
begin
  if GetzLeafDir.Execute then
    begin
     filename := GetzLeafDir.Filename;
     //ShowMessage(filename);
     inputChangedPath.Text:=filename;
    end;
end;

procedure Ttitle_cler_z_leaf_launcher.GroupBox2Click(Sender: TObject);
begin

end;

procedure Ttitle_cler_z_leaf_launcher.inputfieldLinksChange(Sender: TObject);
begin

end;

procedure Ttitle_cler_z_leaf_launcher.inputfieldLinksDragDrop(Sender,
  Source: TObject; X, Y: Integer);
begin

end;

procedure Ttitle_cler_z_leaf_launcher.PageControl2Change(Sender: TObject);
begin
  Form1.Hide;
end;

procedure Ttitle_cler_z_leaf_launcher.pathBrowserExeChange(Sender: TObject);
begin

end;

procedure Ttitle_cler_z_leaf_launcher.RadioButton2Change(Sender: TObject);
begin

end;

procedure Ttitle_cler_z_leaf_launcher.Edit1Change(Sender: TObject);
begin

end;

procedure Ttitle_cler_z_leaf_launcher.GetBrowserDirClick(Sender: TObject);
var filename : string;
begin
  if GetzLeafDir.Execute then
    begin
     filename := GetzLeafDir.Filename;
     //ShowMessage(filename);
     pathBrowserExe.Text:=filename;
    end;
end;

procedure Ttitle_cler_z_leaf_launcher.ReplaceTextButtonClick(Sender: TObject);
var
  s: String;
begin
  s:=inputfieldLinks.Text;
  s:= StringReplace(s, searchForString.Text, replaceStringWithThis.Text, [rfReplaceAll, rfIgnoreCase]);
  inputfieldLinks.Text:=s;
end;

procedure Ttitle_cler_z_leaf_launcher.labelfoundVersClick(Sender: TObject);
begin

end;

procedure Ttitle_cler_z_leaf_launcher.FormDropFiles(Sender: TObject;
  const FileNames: array of String);
var pathToFile:string;
begin
  //ShowMessage(FileNames[0]); // only add the first file
  pathToFile:=FileNames[0];
  //ShowMessage(IntToStr(PageControl2.ActivePageIndex));
  if (pos('.lnk',pathToFile)>0)  then
  begin
    pathToFile:=GetShellLinkTarget(pathToFile);
  end;

  if(PageControl2.ActivePageIndex=0) then // we are on the z-Tree tab
    begin
     if (pos('.exe',pathToFile)>0)  then // the file is an executable. put it into the corresponding path field
          begin
                checkBoxChangedPath.checked:=False; // Show this input field
                inputChangedPath.Text:=pathToFile; // Overwrite the path in the input field
                ShowMessage('Path set to: '+pathToFile);
          end
      else
          begin
               ShowMessage('This seems not be an executable file: '+pathToFile);
          end;

    end
  else  // we are on the zTu tab
    begin
      // if file is exe just put it into the path field otherwise to the link field

      if(pos('.exe',pathToFile)>0) then // the file is an executable. put it into the corresponding path field
          begin
               RadioButton4.checked:=True;
               pathBrowserExe.Text:=pathToFile;
               ShowMessage('Path set to: '+pathToFile);
               //ShowMessage(IntToStr(pos('.exe',pathToFile)));
          end
      else  // Not an executable file. Read only the first 200 lines to prevent a GUI crash..
          begin
             ShowMessage('Reading link-file: '+pathToFile);
             readLinkList(pathToFile);
          end;



    end;
end;

procedure Ttitle_cler_z_leaf_launcher.RadioButton3Change(Sender: TObject);
begin

end;

procedure Ttitle_cler_z_leaf_launcher.RadioButton4Change(Sender: TObject);
begin
  if(RadioButton4.checked=True) then
   begin
    pathBrowserExe.Show;
    GetBrowserDir.Show;
   end
  else
  begin
    pathBrowserExe.Hide;
    GetBrowserDir.Hide;
  end;
end;

procedure Ttitle_cler_z_leaf_launcher.RadioGroup1Click(Sender: TObject);
begin

end;


procedure Ttitle_cler_z_leaf_launcher.writeConfig();
var F: TextFile;
begin
  try
    AssignFile(F, 'cler_leaf_launcher_config.txt');
    try
       Rewrite(F);
       Writeln(F, inputNumLeafs.Text);
       Writeln(F, inputStartWithNumber.Text);
       Writeln(F, inputPrefix.Text);
       Writeln(F, inputFontsize.Text);
       Writeln(F, inputAdditionalParameters.Text);
       Writeln(F, checkBoxChangedPath.Checked);
       Writeln(F, inputChangedPath.Text);
       Writeln(F, checkboxSwitchEntryResolution.Checked);
       Writeln(F, setWidth.Text);
       Writeln(F, setHeight.Text);
       Writeln(F, checkboxCascade.Checked);
       // Second Tab (zTu)
       // Not saving the large input field as not to create confusion when restarting the launcher..
       Writeln(F, searchForString.Text);
       Writeln(F, replaceStringWithThis.Text);
       Writeln(F, RadioButton1.Checked);
       Writeln(F, RadioButton4.Checked);
       Writeln(F, pathBrowserExe.Text);
       // Write the last opened tab into this file
       Writeln(F, IntToStr(PageControl2.ActivePageIndex));

    finally
       CloseFile(F);
    end;
  except
    // File does not exists, just use the defauls set in the form
  end;
  //readFile:=1;
end;

procedure Ttitle_cler_z_leaf_launcher.readConfig();
var
  F: TextFile;
  temp: string;
begin
  try
    AssignFile(F, 'cler_leaf_launcher_config.txt');
    try
       Reset(F);

       temp:='';
       ReadLn(F, temp);
       inputNumLeafs.Text:=temp;
       temp:='';
       ReadLn(F, temp);
       inputStartWithNumber.Text:=temp;
       temp:='';
       ReadLn(F, temp);
       inputPrefix.Text:=temp;
       temp:='';
       ReadLn(F, temp);
       inputFontsize.Text:=temp;
       temp:='';
       ReadLn(F, temp);
       inputAdditionalParameters.Text:=temp;
       temp:='';
       ReadLn(F, temp);
       checkBoxChangedPath.Checked:=StrToBool(temp);
       temp:='';
       ReadLn(F, temp);
       inputChangedPath.Text:=temp;
       temp:='';
       ReadLn(F, temp);
       checkboxSwitchEntryResolution.Checked:=StrToBool(temp);
       temp:='';
       ReadLn(F, temp);
       setWidth.Text:=temp;
       temp:='';
       ReadLn(F, temp);
       setHeight.Text:=temp;
       temp:='';
       ReadLn(F, temp);
       checkboxCascade.Checked:=StrToBool(temp);

       // Second Tab (zTu)
       temp:='';
       ReadLn(F, temp);
       searchForString.Text:=temp;
       temp:='';
       ReadLn(F, temp);
       replaceStringWithThis.Text:=temp;
       temp:='';
       ReadLn(F, temp);
       RadioButton1.Checked:=StrToBool(temp);
       temp:='';
       ReadLn(F, temp);
       RadioButton4.Checked:=StrToBool(temp);
       temp:='';
       ReadLn(F, temp);
       pathBrowserExe.Text:=temp;

       // Write the last opened tab into this file
       temp:='';
       ReadLn(F, temp);
       PageControl2.ActivePageIndex:=StrToInt(temp);

    finally
       CloseFile(F);
    end;
  except
    // File does not exists, just use the defauls set in the form
  end;
  //readFile:=1;
end;

procedure Ttitle_cler_z_leaf_launcher.readLinkList(path:String);
var
  F: TextFile;
  temp: string;
  i: integer = 0;
begin
  try
    AssignFile(F, path);
    try
       Reset(F);
                  // ReadLn(F, temp);
       inputfieldLinks.Text:='';
       while not eof(F) do
        begin
          readln(F, temp);
          inputfieldLinks.Text:=Concat(inputfieldLinks.Text,temp,#13#10);


          if (i > 200) then     // possibly chosen a wrong file. Prevent reading too large files. Useless if file is only one line...
            begin
                 if MessageDlg('Question', 'This file seems to be large. Continue to read? ', mtConfirmation, [mbYes, mbNo, mbIgnore],0) = mrNo then break;
            end;

          i:=i+1;
          //writeln(s);
        end;

    finally
       CloseFile(F);
    end;
  except
    // File does not exists, just use the defauls set in the form
  end;
  //readFile:=1;
end;

procedure Ttitle_cler_z_leaf_launcher.versLabelClick(Sender: TObject);
begin

end;


procedure Ttitle_cler_z_leaf_launcher.stopLeafsClick(Sender: TObject);
begin
  if MessageDlg('Stop running z-Leafs!', 'Are you sure that you want to stop all running z-leaves?', mtConfirmation,[mbYes, mbNo],0) = mrYes then
   begin
       globalErrorOpenzLeaf:=0;
       launchProcess('taskkill /F /IM "zleaf.exe"','Taskkill not found...');
   end;

  //stopLeafs.Hide;
end;

procedure Ttitle_cler_z_leaf_launcher.Button1Click(Sender: TObject);
begin
  Form1.Show;
end;

procedure Ttitle_cler_z_leaf_launcher.Button2Click(Sender: TObject);
begin
   inputNumLeafs.Text:=IntToStr(StrToInt(inputNumLeafs.Text)+1);
end;

procedure Ttitle_cler_z_leaf_launcher.decreaseLeafsClick(Sender: TObject);
begin
   if  StrToInt(inputNumLeafs.Text)<=1 then
     inputNumLeafs.Text:='1'
   else inputNumLeafs.Text:=IntToStr(StrToInt(inputNumLeafs.Text)-1);
end;


procedure Ttitle_cler_z_leaf_launcher.checkBoxChangedPathChange(Sender: TObject);
begin
   if(checkBoxChangedPath.checked=False) then
    begin
     inputChangedPath.Show;
     SelectzLeafDirButton.Show;
    end
   else
   begin
     inputChangedPath.Hide;
     SelectzLeafDirButton.Hide;
   end;
   getzLeafVersion();
end;

procedure Ttitle_cler_z_leaf_launcher.inputChangedPathChange(Sender: TObject);
begin
  getzLeafVersion();
end;



procedure Ttitle_cler_z_leaf_launcher.inputNumLeafsExit(Sender: TObject);
begin
  if  StrToInt(inputNumLeafs.Text)<=1 then
     inputNumLeafs.Text:='1';
end;

procedure Ttitle_cler_z_leaf_launcher.inputStartWithNumberExit(Sender: TObject);
begin
  if  StrToInt(inputStartWithNumber.Text)<=0 then
     inputStartWithNumber.Text:='0';
end;

procedure Ttitle_cler_z_leaf_launcher.setWidthChange(Sender: TObject);
begin

end;

procedure Ttitle_cler_z_leaf_launcher.startLeafsClick(Sender: TObject);
var
  i,start,finalVal,colBreakOffset,xOffset,yOffset,inHeight,inWidth,xpos,ypos,xResolution,yResolution: Integer;
  commandstring, tempString, screenResolution,additionalFlags: String;
  startClients: Boolean;
begin
    writeConfig();
    startClients:=True; // As the check goes to a second level... Could be accomplished with an exception statement...
    if  ((StrToInt(inputStartWithNumber.Text)=oldinputNumLeafs) and (inputPrefix.Text=oldinputPrefix)) then
    begin
       if MessageDlg('Remember to save your work!', 'It seems, that you are trying to start new z-Leafs with the same client names.'#10''#10'Are you sure you want to start?', mtConfirmation,[mbYes, mbNo],0) = mrNo then
       begin
           startClients:=False;
       end;
    end;

    oldinputNumLeafs:=StrToInt(inputStartWithNumber.Text);
    oldinputPrefix:=inputPrefix.Text;

    if startClients=True then
    begin
      colBreakOffset:=100;
      commandstring:=Concat('"',GetCurrentDir);

      if checkBoxChangedPath.checked=True then
         commandstring:=Concat(commandstring,'\zleaf.exe" ')
      else
         commandstring:=Concat('"',inputChangedPath.Text,'" ');

      additionalFlags:='';
      additionalFlags:=inputAdditionalParameters.Text;
      screenResolution:=Concat('/size ',setWidth.Text,'x',setHeight.Text);

      xResolution := Screen.WorkAreaRect.Right - Screen.WorkAreaRect.Left;
      yResolution := Screen.WorkAreaRect.Bottom - Screen.WorkAreaRect.Top;

      // Presets
      xOffset:=10; // Set the x offset
      yOffset:=20;
      xpos:=0;
      ypos:=0;

      // the y offset is calculated

      inHeight := StrToInt(setHeight.Text);
      inWidth  := StrToInt(setWidth.Text);

      // 1. Check wether the entered resolution is smaller than the screen. Otherwise open in fullscreen
      // Note that the point 0,0 is in the top left corner.
      if ((inHeight<(yResolution)) and (inWidth<(xResolution))) then
      begin
        yOffset:=floor(2*(yResolution - inHeight) / (StrToInt(inputNumLeafs.Text)));  // Calculate the y offset between each zLeaf
        // Calculate the offset between the two cascades such that the second column finishes on the right screen border
        // I use the same y and x offset to create the cascade as this looks harmonic
        colBreakOffset:=xResolution - inWidth - yOffset*StrToInt(inputNumLeafs.Text);
      end;
         // stopLeafs.Show;
      start:=StrToInt(inputStartWithNumber.Text);
      finalVal:=start+StrToInt(inputNumLeafs.Text)-1;

      globalErrorOpenzLeaf:=0; // Reset the errorvariable in order to prevent multiple error messages
      for i := start to finalVal do
      begin
           if checkboxSwitchEntryResolution.checked=True then
           begin  // open the windows in fullscreen
              tempString:=Concat(commandstring,' /name client',IntToStr(i),' ',additionalFlags) ;
              launchProcess(tempString,'zLeaf could not be found in this directory...');
           end
           else
           begin  // Do not use the Fullscreen mode
              if checkboxCascade.Checked=True then
              begin
               if ypos + inHeight < yResolution  then
               begin // In case the Leaf still fits onto the working area
                 tempString:=Concat(commandstring,screenResolution,' /position ',IntToStr(xpos),',',IntToStr(ypos),' /name client',IntToStr(i),' ',additionalFlags) ;
                 launchProcess(tempString,'zLeaf could not be found in this directory...');
                 ypos:=ypos+yOffset;
                 xpos:=xpos+yOffset;
               end
               else
               begin
                 // Start the second cascade
                 ypos:=Floor(yOffset/4); //A vertical offset such that the columns are not aligned
                 xpos:=xpos+ colBreakOffset; // Create a larger gap between the two cascades
                 tempString:=Concat(commandstring,screenResolution,' /position ',IntToStr(xpos),',',IntToStr(ypos),' /name client',IntToStr(i),' ',additionalFlags) ;
                 launchProcess(tempString,'zLeaf could not be found in this directory...');
                 ypos:=ypos+yOffset;
                 xpos:=xpos+yOffset;
               end;
              end
              else
              begin
                 // use the custom resolution without the cascade
                 tempString:=Concat(commandstring,screenResolution,' /name client',IntToStr(i),' ',additionalFlags) ;
                 launchProcess(tempString,'zLeaf could not be found in this directory...');
              end;
           end;
      end;
    end;
end;


procedure Ttitle_cler_z_leaf_launcher.startBrowser(param:String; errormsg:String);
var
  s: String;
  tempString:String;
begin
  s:='';
  tempString:='';
  globalErrorOpenzLeaf:=0; // Reset the errorvariable in order to prevent multiple error messages
  for s in inputfieldLinks.Lines do
  begin
    if (length(s)>0) then    // exclude empty lines
    tempString:=Concat('"',param,'" ',s);      // Create the start command
    //ShowMessage(tempString);
    launchProcess(tempString,errormsg);
  end;

end;

procedure Ttitle_cler_z_leaf_launcher.startzLeafsInBrowserClick(Sender: TObject);
var
  s: String;
begin
  writeConfig();
  if(length(inputfieldLinks.Text))>0 then // do something, only if some links are entered..
  begin
    s:='';
    if (RadioButton1.Checked=True) then   // chrome is selected
      begin

       globalErrorOpenzLeaf:=0; // Reset the errorvariable in order to prevent multiple error messages
       for s in inputfieldLinks.Lines do
       begin
         if (length(s)>0) then    // exclude empty lines
         OpenURL(s);       // Open the link in the default browser
         Sleep(250);
       end;
      end
      else if (RadioButton4.Checked=True) then // Other is selected
      begin
        if (length(pathBrowserExe.Text)<1) then
        begin
           ShowMessage('You need to specify a path to the executable of your browser...');
        end
        else
        begin // Open the links in a custom browser
          startBrowser(pathBrowserExe.Text,'Could not start your browser... Check your path to the executable.');
        end;

      end
    else
      begin
        ShowMessage('Error, nothing selected...');
      end;
   end
  else  // in case no links were entered into the memofield 'inputfieldLinks'
   begin
      ShowMessage('You need to enter links in order to start them...');
   end;

end;


procedure Ttitle_cler_z_leaf_launcher.checkboxSwitchEntryResolutionChange(Sender: TObject);
begin
  if checkboxSwitchEntryResolution.Checked=True Then
  begin
    setResolution.Hide;
    setWidth.Hide;
    setHeight.Hide;
    setDelimiter.Hide;
    labelCascade.Hide;
    checkboxCascade.Hide;
  end
  else
  begin
    setResolution.Show;
    setWidth.Show;
    setHeight.Show;
    setDelimiter.Show;
    labelCascade.Show;
    checkboxCascade.Show;
  end;
end;

procedure Ttitle_cler_z_leaf_launcher.inputPrefixChange(Sender: TObject);
begin

end;

procedure Ttitle_cler_z_leaf_launcher.GroupBox1Click(Sender: TObject);
begin

end;

procedure Ttitle_cler_z_leaf_launcher.loadConfigClick(Sender: TObject);
begin

end;

procedure Ttitle_cler_z_leaf_launcher.labelStartWithNumberClick(Sender: TObject);
begin

end;

procedure Ttitle_cler_z_leaf_launcher.labelPrefixClick(Sender: TObject);
begin

end;

procedure Ttitle_cler_z_leaf_launcher.labelFontsizeClick(Sender: TObject);
begin

end;

procedure Ttitle_cler_z_leaf_launcher.labelNumLeafsClick(Sender: TObject);
begin

end;





end.


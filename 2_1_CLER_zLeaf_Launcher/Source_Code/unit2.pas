unit Unit2;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Unit1;

type

  { TForm1 }

  TForm1 = class(TForm)
    buttonAddLanguage: TButton;
    buttonAddChannel: TButton;
    buttonAddServer: TButton;
    ComboBoxLanguage: TComboBox;
    inputServer: TEdit;
    inputChannel: TEdit;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    labelChannel: TLabel;
    labelServer: TLabel;
    Label5: TLabel;
    labelAddParams: TLabel;
    procedure buttonAddChannelClick(Sender: TObject);
    procedure buttonAddLanguageClick(Sender: TObject);
    procedure buttonAddServerClick(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure Edit2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Label5Click(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;
const
  // An array with all languages. From the z-Tree manual p. 205f
  languages: array[0..31, 0..1] of string = (('Arabic','arabic'),('Bahasa Indonesia','indonesian'),('Brasil','brasil'),('Catalan','catalan'),('Chinese','chinese'),('Croatian','croatian'),('Czech','czech'),('Danish','danish'),('Dutch','dutch'),('English','english'),('Finnish','finnish'),('French','french'),('German','german'),('Greek','greek'),('Hebrew','hebrew'),('Italian','italian'),('Japanese','japanese'),('Magyar','hungarian'),('Melayu','malay'),('Mongolian','mongolian'),('Norwegian - Bokmål','norwegian'),('Norwegian - Nynorsk','newnorwegian'),('Polish','polish'),('Portugues','portugues'),('Russian','russian'),('Slovenian','slovak'),('Spanish','spanish'),('Swedish','swedish'),('Swiss German','zurichgerman'),('Turkish','turkish'),('Ukraine','ukraine'),('Vietnamese','vietnamese'));

implementation

{$R *.lfm}

{ TForm1 }



procedure TForm1.FormCreate(Sender: TObject);
var
  lang: array of string;
begin
  ComboBoxLanguage.Items.Clear;             //Alle vorhandenen Auswahlmöglichkeiten löschen
  for lang in languages do
    ComboBoxLanguage.Items.Add(Concat(lang[0],' - ', lang[1]));        //Add the language

  ComboBoxLanguage.ItemIndex := 12; // Preselect German
  //title_cler_z_leaf_launcher.inputPrefix.Text:='asdasd';
end;

procedure TForm1.Label5Click(Sender: TObject);
begin

end;

procedure TForm1.Edit2Change(Sender: TObject);
begin

end;

procedure TForm1.buttonAddLanguageClick(Sender: TObject);
var
  wspace: string;
begin
  if title_cler_z_leaf_launcher.inputAdditionalParameters.Text='' then
    wspace:=''
  else wspace:=' ';

  title_cler_z_leaf_launcher.inputAdditionalParameters.Text:=Concat(title_cler_z_leaf_launcher.inputAdditionalParameters.Text,wspace,'/language ',languages[ComboBoxLanguage.ItemIndex][1]);
  //title_cler_z_leaf_launcher.inputPrefix.Text:=Concat(title_cler_z_leaf_launcher.inputPrefix.Text,' ',IntToStr(ComboBoxLanguage.ItemIndex));
end;

procedure TForm1.buttonAddChannelClick(Sender: TObject);
var
  wspace: string;
begin
  if title_cler_z_leaf_launcher.inputAdditionalParameters.Text='' then
    wspace:=''
  else wspace:=' ';
  if not(inputChannel.Text='') then
     title_cler_z_leaf_launcher.inputAdditionalParameters.Text:=Concat(title_cler_z_leaf_launcher.inputAdditionalParameters.Text,wspace,'/channel ',inputChannel.Text);
end;

procedure TForm1.buttonAddServerClick(Sender: TObject);
var
  wspace: string;
begin
  if title_cler_z_leaf_launcher.inputAdditionalParameters.Text='' then
    wspace:=''
  else wspace:=' ';
  if not(inputServer.Text='') then
     title_cler_z_leaf_launcher.inputAdditionalParameters.Text:=Concat(title_cler_z_leaf_launcher.inputAdditionalParameters.Text,wspace,'/server ',inputChannel.Text);
end;

procedure TForm1.Edit2Click(Sender: TObject);
begin

end;

end.


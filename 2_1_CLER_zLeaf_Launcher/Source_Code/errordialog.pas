unit ErrorDialog;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TDuplicatedStartWarning }

  TDuplicatedStartWarning = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Memo1: TMemo;
    procedure Button2Click(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
  private

  public

  end;

var
  DuplicatedStartWarning: TDuplicatedStartWarning;

implementation

{$R *.lfm}

{ TDuplicatedStartWarning }

procedure TDuplicatedStartWarning.Memo1Change(Sender: TObject);
begin

end;

procedure TDuplicatedStartWarning.Button2Click(Sender: TObject);
begin

end;

end.


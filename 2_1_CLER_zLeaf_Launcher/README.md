# CLER_zLeaf_Launcher

**This is an archived version. Please visit https://gitlab.com/cler1/cler_ztree_launcher for the latest version!**

The cler_zTree_Launcher is lightweight and makes the starting of an arbitrary
number of z-Leafs simple and is costumizable. Just place the batch file into the 
path where your zleaf.exe is located and execute it. 


# Build from source

In order to complie the source code, please download the Lazarus IDE: 
https://www.lazarus-ide.org/index.php?page=downloads

Please note, that you'll need to run it on Windows.

